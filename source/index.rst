Debian Developer's Reference
********************************************************************************************************************************

Developer's Reference Team <developers-reference@packages.debian.org>

* Copyright © 2019 - 2020 Holger Levsen
* Copyright © 2015 - 2020 Hideki Yamane
* Copyright © 2008 - 2015 Lucas Nussbaum
* Copyright © 2004 - 2007 Andreas Barth
* Copyright © 2002 - 2009 Raphaël Hertzog
* Copyright © 1998 - 2003 Adam Di Carlo
* Copyright © 1997 - 1998 Christian Schwarz

This manual is free software; you may redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2, or (at your option) any later version.

This is distributed in the hope that it will be useful, but without any
warranty; without even the implied warranty of merchantability or fitness for a
particular purpose. See the GNU General Public License for more details.

A copy of the GNU General Public License is available as
/usr/share/common-licenses/GPL-2 in the Debian distribution or on the World
Wide Web at the GNU web site. You can also obtain it by writing to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.

This is Debian Developer's Reference version |VERSION|\ , 
released on |PUBDATE|\ .

If you want to print this reference, you should use the pdf version. This
manual is also available in some other languages.

.. toctree::
   :maxdepth: 4
   :numbered:

   scope
   new-maintainer
   developer-duties
   resources
   pkgs
   best-pkging-practices
   beyond-pkging
   l10n


.. toctree::
   :caption: Appendix
   :name: appendix
   :maxdepth: 4
   :numbered:

   tools

